import React from 'react';
import ReactDOM from 'react-dom';

class Checkout extends React.Component {
	constructor(props) {
	    super(props);
		var state = {};

	    this.props.products.forEach(
	    	function(product) {
	      		state[product.name] = product.quantity;
	    	}
	    );

	    // Init state
	    this.state = {
	      quantities: state 
	    };

	    // Bind handler
    	this.handleChange = this.handleChange.bind(this);
  	}

	handleChange(quantity, name) {
	    var items;

	   	items = this.state.quantities;
	   	items[name] = quantity;

	   	this.setState({quantities: items});
	}

  render() {
    var rows = [];
    var formClasses = `pnForm accountForm`;
    var form;
    var that = this;
    var totalPrice = 0;

    this.props.products.forEach(
    	function(product) {
      		rows.push(<ProductRow product={product} key={product.name} onChange={that.handleChange}/>);
      		totalPrice += parseInt(that.state.quantities[product.name]) * product.price;
    	}
    );
    
    totalPrice = totalPrice.toFixed(2);

    return ( 
		<form className={formClasses} action="#" name="checkoutForm">       
	    	<div className="fakeTable">
                <div className="fakeThead">
                	<div className="fakeTd productTh"><span className="label">Product</span></div>
                    <div className="fakeTd"><span className="label">Quantity</span></div>
                    <div className="fakeTd"><span className="label">Total</span></div>
                    <div className="fakeTd removeTd"><span className="label">Remove</span></div>
	    		</div>
	    		{rows}
	    	</div>
	    	<CheckoutFooter total={totalPrice}/>
	    </form> 
	)
  }
}

class CheckoutFooter extends React.Component {
	render() {
		return ( 
			<div className="formFooter">
                <div className="checkout">
                    <div className="priceTotal">
                        <div className="priceWrap">
                            <h2 className="prodSubtitle">Total</h2>
                            <div className="currentPrice price">
                                <span>{this.props.total}</span>
                                <span>€</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
	}
}

class ProductRow extends React.Component {  
	constructor(props) {
	    super(props);
	    this.state = {
	      quantity: this.props.product.quantity
	    };

    	this.handleUserChange = this.handleUserChange.bind(this);
  	}

	handleUserChange(quantity) {
	    this.setState({
	      quantity: quantity
	    });

	    this.props.onChange(quantity, this.props.product.name);
	}

  	render() {
  		if (this.state.quantity == 0 ){
  			return false;
  		} else {
			var productImage = `fakeTd productImage`;
			var productName = `fakeTd productTd`;
	  		var totalPrice = (this.props.product.price * this.state.quantity).toFixed(2);

	    	return (
	    		<div className="fakeTr">
	                <div className="trWrap">
	                    <div className={productImage}>
	                        <img src={this.props.product.image}/>
	                    </div>
	                    <div className={productName}>
	                        <h3 className="title4">{this.props.product.name}</h3>
	                        <strong>{this.props.product.price} €</strong>
	                    </div>
	                    <Spinner quantity={this.state.quantity} onUserChange={this.handleUserChange}/>
	                    <Price totalPrice={totalPrice} />
	                    <Remove onClick={this.handleUserChange}/>
	                </div>
	            </div>
	    	)
  		}
  	}
}	

class Spinner extends React.Component {
	constructor(props) {
	    super(props);
	    this.clickIncrease = this.clickIncrease.bind(this);
	    this.clickDecrease = this.clickDecrease.bind(this);
	    this.handleChange = this.handleChange.bind(this);
	  }

	clickIncrease() {
	    this.props.onUserChange(
	      	parseInt(this.quantity.value) + 1
	    );
	  }

	clickDecrease() {
		if (this.quantity.value - 1 <= 0) return false;

	    this.props.onUserChange(
	      parseInt(this.quantity.value) - 1
	    );
	  }

	handleChange() {
		if (this.quantity.value <= 0) return false;
	    this.props.onUserChange(
	      this.quantity.value
	    );
	  }
	  
  	render() {
  		var spinnerSpan = `ui-spinner ui-corner-all ui-widget ui-widget-content`,
  			inputClasses = `spinnerInput ui-spinner-input`,
  			increaseClasses = `ui-button ui-widget ui-spinner-button ui-spinner-up ui-corner-tr ui-button-icon-only`,
  			increaseSpan = `ui-button-icon ui-icon ui-icon-triangle-1-n`,
  			decreaseClasses = `ui-button ui-widget ui-spinner-button ui-spinner-down ui-corner-br ui-button-icon-only`,
  			decreaseSpan = `ui-button-icon ui-icon ui-icon-triangle-1-s`;

  		var spanStyle = {
  			height: '45px'
  		};

    	return (
    		<div className="fakeTd">
	    		<div className="qSpinner">
	    			<span className={spinnerSpan} style={spanStyle}>
		    			<input className={inputClasses} id="spinner-1" name="spinner-1" required="" aria-valuemin="1" aria-valuenow="1" role="spinbutton" aria-required="true" 
		    				value={this.props.quantity} ref={(input) => this.quantity = input} onChange={this.handleChange}/>
		    			<a onClick={this.clickDecrease} aria-hidden="true" 
		    				className={decreaseClasses} role="button">
		    				<span className={decreaseSpan}></span>
		    				<span className="ui-button-icon-space"> </span>
		    			</a>
		    			<a onClick={this.clickIncrease} aria-hidden="true" 
		    			className={increaseClasses}role="button">
		    				<span className={increaseSpan}></span>
		    				<span className="ui-button-icon-space"> </span>
		    			</a>
	    			</span>
	            </div>
            </div>
    	)
  	}
}	

class Price extends React.Component {
  	render() {
		var productPrice = `fakeTd productPrice`;
    	
    	return (
            <div className="fakeTd">
                <strong>{this.props.totalPrice} €</strong>
            </div>
    	)
  	}
}	

class Remove extends React.Component {
	constructor(props) {
	    super(props);
	    this.remove = this.remove.bind(this);
	}

	remove() {
	    this.props.onClick(0);
	}

  	render() {
		var productRemove = `fakeTd removeProduct removeTd`;

    	return (
    		<div onClick={this.remove} className={productRemove}>
                <div className="spangica">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
    	)
  	}
}	

var PRODUCTS = [
  {price: '49.99', image: 'img/football.png', name: 'Football', quantity: 2},
  {price: '9.99', image: 'img/baseball.jpg', name: 'Baseball', quantity: 1},
  {price: '29.99', image: 'img/basketball.jpg', name: 'Basketball', quantity: 3}
];

ReactDOM.render( 
	<Checkout products={PRODUCTS} />,
    document.getElementById('app')
);